import psycopg2
from psycopg2 import sql

# Подключение к базе данных
conn = psycopg2.connect(
    database="postgres",
    user="postgres",
    password="postgresql",
    host="database",
    port="5432"
)
cursor = conn.cursor()

# Выполнение запроса к таблице students, где поле marks = 2
select_query = '''
    SELECT DISTINCT name FROM students RIGHT JOIN markbooks ON students.id = markbooks.student_id WHERE mark = %s
'''
cursor.execute(select_query, (2,))

# Получение результатов запроса
rows = cursor.fetchall()

# Вывод результатов
for row in rows:
    print(row)

# Закрытие соединения с базой данных
conn.close()