CREATE TABLE students (
    id SERIAL PRIMARY KEY,
    name TEXT,
    birth_date DATE
);

CREATE TABLE markbooks (
    id SERIAL PRIMARY KEY,
    student_id INTEGER REFERENCES students(id),
    subject TEXT,
    teacher TEXT,
    mark SMALLINT,
    exam_date DATE
);

-- Вставка тестовых данных
INSERT INTO students (name, birth_date) VALUES
    ('Иванов Иван', '2000-01-01'),
    ('Петров Петр', '1999-05-15'),
    ('Сидорова Анна', '2001-08-20');

-- Вставка тестовых данных
INSERT INTO markbooks (student_id, subject, teacher, mark, exam_date) VALUES
    (1, 'Математика', 'Иванова Н.П.', 4, '2023-01-15'),
    (2, 'Физика', 'Смирнов С.И.', 5, '2023-02-10'),
    (3, 'Химия', 'Кузнецова Е.В.', 2, '2023-03-05'),
    (2, 'Химия', 'Кузнецова Е.В.', 2, '2023-03-05'),
    (1, 'Химия', 'Кузнецова Е.В.', 2, '2023-03-05');
